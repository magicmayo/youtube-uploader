const express = require("express");
const readline = require('readline');
const {createReadStream, rename} = require('fs');
const multer = require('multer');
const authUpload = require('./auth.js');
const uploads = multer({dest: 'videos/'});

const app = express();

app.use(express.json());
app.use(express.static("public"));

const params = ({title, description, privacy, newName}) => {
    return ({
    part: 'id,snippet,status',
    notifySubscribers: false,
    requestBody: {
        snippet: {
            title: title,
            description: description,
        },
        status: {
            privacyStatus: privacy
        }
    },
    media: {
        body: createReadStream(newName)
    }
});}

const options = size => ({
    onUploadProgress: evt => {
        const progress = (evt.bytesRead / size) * 100;
        readline.clearLine(process.stdout, 0);
        readline.cursorTo(process.stdout, 0, null);
        process.stdout.write(`${Math.round(progress)}% complete`);
    }
});


app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/public/index.html`);
});

app.post('/upload', uploads.single('file'), ({body, file}, res) => {
    const newName = `videos/${body.title}.${file.originalname.split('.')[file.originalname.split('.').length - 1]}`;

    rename(file.path, newName, (err) => {
        if(err) throw err;

        authUpload(params({...body, newName}), options(file.size))
        .then(upload => res.json(upload))
        .catch(err => res.json({err, newName}));
    });
});

app.post('/retry', ({body}, res) => {
    authUpload(params(body), options(body.fileSize)).then(upload => res.json(upload)).catch(err => res.json({err, newName: body.newName}));
});

app.listen(3030);
document.body.onload = () => {
    const video = document.querySelector('#video')
    const form = document.querySelector('#video-upload');
    const description = document.querySelector('description');
    const title = document.querySelector('#title');
    const vidWrapper = document.querySelector("#videoSourceWrapper");
    const vidPlayer = document.querySelector("#videoPlayer");
    const source = document.querySelector('#videoSource');
    const privacy = document.querySelector('#privacy');
    const modal = document.querySelector('.modal-wrapper');

    const retryUpload = async (retryVideo, vidDetails) => {
        document.querySelector('#retry').setAttribute('disabled', 'true');

        const body = {
            newName: retryVideo,
            ...vidDetails
        }

        const retry = await fetch('/retry', {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        response(retry);
    }

    const response = async (req, vidDetails) => {
        const {err, newName, data} = await req.json();

        console.log(err)
        if(err.errors && err.errors.length > 0){
            // if(err.code == 401 || err.code == 403){
            //     return showModal(false, err.errors[0].message)
            // }

            return showModal(false, err.errors[0].message, vidDetails, newName);
        }

        return showModal(true, data.id)
    }

    const showModal = (status, content, vidDetails, retry = false) => {
        modal.innerHTML = '';
        const inner = document.createElement('p');
        const button = document.createElement('button');
        const buttonwWrapper = document.createElement('div');
        let retryButton = '';

        if(retry){
            retryButton = document.createElement('button');
            retryButton.type = 'button';
            retryButton.innerText = 'Retry';
            retryButton.setAttribute('id', 'retry');
            retryButton.addEventListener('click', () => retryUpload(retry, vidDetails));
        }

        button.type = 'button';
        button.innerText = status ? 'Go to video' : 'OK';
        button.addEventListener('click', () => status ? window.open(`https://youtu.be/${content}`, '_blank').focus() : modal.classList.add('hide'));

        inner.innerHTML = status ? `Video was uploaded!`: `Upload failed: ${content}`;

        buttonwWrapper.append(button, retryButton);
        modal.append(inner, button, buttonwWrapper);
        modal.classList.remove('hide');
    }

    const upload = async e => {
        e.preventDefault();

        const file = new FormData();
        file.append('privacy', privacy.value);
        file.append('title', title.value || video.files[0].name.split(' ').join(''));
        file.append('description', description || '');
        file.append('file', video.files[0]);

        const req = await fetch('/upload', {
            method: 'POST',
            body: file
        });

        return response(req, {privacy: privacy.value, title: title.value || video.files[0].name.split(' ').join(''), description: description || '', fileSize: video.files[0].size});
    }

    const addVideo = e => {
        const file = e.target.files[0];
        source.setAttribute('type', file.type);
        source.src = URL.createObjectURL(file);
        vidPlayer.load();
        vidWrapper.classList.remove('hide');
    }

    video.addEventListener('change', addVideo);
    form.addEventListener('submit', upload);
}